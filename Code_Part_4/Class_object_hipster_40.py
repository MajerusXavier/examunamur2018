from random import randint
from Code_Part_3.PriceFunction import fprices, fprices2015, dprices, dprices2015

class Customer(object):
    def __init__(self, returnlist, uniquelist):
        self.returnlist = returnlist
        self.uniquelist = uniquelist

# We only changed hipster's budget
    def rbud(self):
        for client in self.returnlist:
            if client.hipster:
                client.rbud(40.0)
            elif client.regular:
                client.rbud(250.0)

    def ubud(self):
        for client in self.uniquelist:
            client.ubud(100.0)

    def rleftbud(self, drink, food):
        for client in self.returnlist:
            lbud = client.budget - dprices(drink) - fprices(food)
            client.rbud(lbud)

    def rleftbud2015(self, drink, food):
        for client in self.returnlist:
            lbud = client.budget - dprices2015(drink) - fprices2015(food)
            client.rbud(lbud)

    def uleftbud(self, drink, food):
        for client in self.uniquelist:
            lbud = client.budget - dprices(drink) - fprices(food) - client.tip
            client.ubud(lbud)

    def uleftbud2015(self, drink, food):
        for client in self.uniquelist:
            lbud = client.budget - dprices2015(drink) - fprices2015(food) - client.tip
            client.ubud(lbud)

    def utips(self):
        for client in self.uniquelist:
            if client.tripadvisor:
                tip = randint(1, 10)
                client.utips(float(tip))
            else:
                tip = 0.0
                client.utips(tip)

    def rtips(self):
        for client in self.returnlist:
            tip = 0.0
            client.rtips(tip)

    def rtype(self):
        for client in self.returnlist:
            if client.hipster:
                type = "hipster"
            elif client.regular:
                type = "regular"
            client.rtype(type)

    def utype(self):
        for client in self.uniquelist:
            if client.regular:
                type = "regular"
            else:
                type = "tripadvisor"
            client.utype(type)


class Returning(object):
    def __init__(self, hipster, regular):
        self.hipster = hipster
        self.regular = regular

    def rbud(self, budget):
        self.budget = budget

    def rtype(self, type):
        self.type = type

    def rtips(self, tip):
        self.tip = tip


class Unique(object):
    def __init__(self, regular, tripadvisor):
        self.regular = regular
        self.tripadvisor = tripadvisor

    def ubud(self, budget):
        self.budget = budget

    def utips(self, tip):
        self.tip = tip

    def utype(self, type):
        self.type = type


