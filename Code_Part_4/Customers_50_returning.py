from random import randint
from Code_part_2.Class_object import Customer, Returning, Unique

# creating returning customers
return_list = []

#
for i in range(50):
    if randint(1, 9) <= 3:
        hipster = True
        regular = False
    else:
        hipster = False
        regular = True

    return_list.append(Returning(hipster, regular))

# Creating unique customers

unique_list = []

for s in range(312075):
    if randint(1, 10) <= 1:
        tripadvisor = True
        regular = False
    else:
        tripadvisor = False
        regular = True
    unique_list.append(Unique(regular, tripadvisor))

# Applying budget to customers
Customer(return_list, unique_list).ubud()
Customer(return_list, unique_list).rbud()

# Applying type to customers
Customer(return_list, unique_list).utype()
Customer(return_list, unique_list).rtype()

# Applying tips to customers
Customer(return_list, unique_list).utips()
Customer(return_list, unique_list).rtips()

# Functions for simulation
def returnlist():
    return return_list

def returnList(nbr):
    return return_list[nbr]


def uniqueList(nbr):
    return unique_list[nbr]


def rebudget(nbr):
    return return_list[nbr].budget


def retype(nbr):
    return return_list[nbr].type


def retip(nbr):
    return return_list[nbr].tip


def unbudget(nbr):
    return unique_list[nbr].budget


def untype(nbr):
    return unique_list[nbr].type


def untip(nbr):
    return unique_list[nbr].tip



