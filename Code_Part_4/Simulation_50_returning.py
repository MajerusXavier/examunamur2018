import random as rd
import pandas as pd
import numpy as np
from Code_part_2.Class_object import Customer
from Code_Part_4.Customers_50_returning import unbudget, untype, untip,  uniqueList, returnlist
from Code_Part_3.ApplyProb import applydprobdrink, applyprobfood

############
#  3'24"  #
###########

DataSet = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";").\
    rename(columns={'TIME': 'time', 'CUSTOMER': 'customer', 'DRINKS': 'drinks', 'FOOD': 'food'})

DataSet['food'].fillna("nothing", inplace=True)

# let's create a dataframe with list

# Column time
simulation = pd.DataFrame({"time": DataSet['time']})


# Columns drink and food
drink = []
food = []

for i in DataSet['time'].str[11:16]:
    drink.append(applydprobdrink(i))
    food.append(applyprobfood(i))

simulation['drinks'] = drink
simulation['food'] = food


# Columns id, leftbudget, tip and type
id = []
leftbudget = []
tip = []
type = []

next = 0

returnlist = returnlist()
returnlist2 = returnlist.copy()

for r, s in enumerate(simulation['time']):
    if len(returnlist2) >= 1 & np.random.binomial(1, 0.2) == 1:
        p = rd.randint(0, (len(returnlist2)-1))

        Customer([returnlist2[p]], [uniqueList(p)]).rleftbud(simulation['drinks'][r], simulation['food'][r])

        id.append("R" + str(next))
        leftbudget.append(returnlist2[p].budget)
        tip.append(returnlist2[p].tip)
        type.append(returnlist2[p].type)

        if returnlist2[p].budget < 10:
            returnlist2.pop(p)

    else:
        Customer([returnlist[1]], [uniqueList(next)]).uleftbud(simulation['drinks'][r], simulation['food'][r])

        id.append("U"+str(next))
        leftbudget.append(unbudget(next))
        tip.append(untip(next))
        type.append(untype(next))

        next += 1


simulation['id'] = id
simulation['lefbudget'] = leftbudget
simulation['tip'] = tip
simulation['type'] = type

# Save our simulation on a csv file
simulation.to_csv("../Data/50_returning.csv", sep=';', index=False)
