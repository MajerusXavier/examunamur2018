import pandas as pd

simulation = pd.read_csv("../Data/DataSimulation.csv", sep=";")

# To get histories
histories = simulation[['id', 'time', 'drinks', 'food']].sort_values(by=['id', 'time'])

print(histories[histories['id'] == "R0"])