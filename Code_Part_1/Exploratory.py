import pandas as pd
import matplotlib.pyplot as plt

# Rename columns
DataSet = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";").\
    rename(columns={'TIME': 'time', 'CUSTOMER': 'customer', 'DRINKS': 'drinks', 'FOOD': 'food'})

# What food and drinks are sold by the coffee bar? How many unique customers did the bar have?
DataSet['time'].count()


drink_list = []
food_list = []

for i in DataSet['drinks']:
    if i not in drink_list:
        drink_list.append(i)

for i in DataSet['food']:
    if i not in food_list:
        food_list.append(i)

# To exclude 'nan' from our food_list
food_list.pop(0)

# Showing sold food/drinks and number of unique customers
print("Available drinks are: %s " % (drink_list))
print("Available food is: %s" %(food_list))


print("There are %s unique customers" % (len(DataSet['customer'].unique())))  # 247988

# Making a bar plot of the total amount of sold foods  and drinks over the five years

byfood = DataSet.groupby('food', as_index=False).count()
x = byfood['food']
y = byfood['time']

    # To show values of variable
plt.bar(x, y)
for a, b in zip(x, y):
    plt.text(a, b, str(b), color='green')

plt.title('Total amount of sold foods over the five years')
plt.show()



bydrink = DataSet.groupby('drinks', as_index=False).count()
x2 = bydrink['drinks']
y2 = bydrink['time']

    # To show values of variable
plt.bar(x2, y2)
for a, b in zip(x2, y2):
    plt.text(a, b, str(b), color='green')

plt.title('Total amount of sold drinks over the five years')
plt.show()


# Determine the average that a customer buys a certain food or drink at any given time

    # To keep only hours from time  and replace 'nan' values

DataSet['hours'] = DataSet['time'].str[11:16]
DataSet['food'].fillna("nothing", inplace=True)

    # probability in series


ProbDrink = (DataSet.groupby(['hours', 'drinks']).count() / DataSet.groupby('hours').count())['time']
ProbFood = (DataSet.groupby(['hours', 'food']).count() / DataSet.groupby('hours').count())['time']

ProbFood = ProbFood.unstack().fillna(0)
ProbDrink = ProbDrink.unstack().fillna(0)


