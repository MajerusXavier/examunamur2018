import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

# PLOTS OF SOLD FOOD PER HOUR #

DataSet = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";").\
    rename(columns={'TIME': 'time', 'CUSTOMER': 'customer', 'DRINKS': 'drinks', 'FOOD': 'food'})

DataSet['hours'] = DataSet['time'].str[11:16]
DataSet['food'].fillna("nothing", inplace=True)

# 1°) Plot of drink per hour
    # Group drinks and hours independently
bydrink = (DataSet.groupby(['hours', 'drinks'])['time'].count())
bydrink = bydrink.unstack().fillna(0)

    # Classifying of data according type of drinks:
x = bydrink.index[::20]
y = bydrink['coffee'][::20]
y2 = bydrink['soda'][::20]
y3 = bydrink['tea'][::20]
y4 = bydrink['water'][::20]
y5 = bydrink['frappucino'][::20]
y6 = bydrink['milkshake'][::20]


    # Building a plot with a specific legend for interesting outcomes
plt.plot(x, y)
plt.plot(x, y2)
plt.plot(x, y3)
plt.plot(x, y4)
plt.plot(x, y5)
plt.plot(x, y6)

    # Making a legend
blue_patch = mpatches.Patch(color='blue', label='coffee')
orange_patch = mpatches.Patch(color='orange', label='soda')
plt.legend(handles=[blue_patch, orange_patch])

plt.title("Total sold drink per hour")

plt.show()


# 2°) Plot of food per hour

    # Group food and hours independently
byfood = (DataSet.groupby(['hours', 'food'])['time'].count())
byfood = byfood.unstack().fillna(0)

    # Classifying of data according type of food
z = byfood.index[::20]
w = byfood['nothing'][::20]
w2 = byfood['sandwich'][::20]
w3 = byfood['cookie'][::20]
w4 = byfood['muffin'][::20]
w5 = byfood['pie'][::20]

    # Building a plot with a specific legend for interesting outcomes
plt.plot(z, w)
plt.plot(z, w2)
plt.plot(z, w3)
plt.plot(z, w4)
plt.plot(z, w5)

    # Making a legend
blue_patch = mpatches.Patch(color='blue', label='nothing')
orange_patch = mpatches.Patch(color='orange', label='sandwich')
plt.legend(handles=[blue_patch, orange_patch])

plt.title("Total sold food per hour")

plt.show()

