import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

simulation = pd.read_csv("../Data/DataSimulation.csv", sep=";")
simulation2 = pd.read_csv("../Data/Hipster_40.csv", sep=";")

# Number of monthly hipster with budget of 500
simulation['month'] = simulation['time'].str[:7]

byhipster = simulation.groupby(by=['month','type']).count()
byhipster = byhipster.unstack()['time']

x = byhipster.index[::4]
y = byhipster['hipster'][::4]

plt.plot(x, y)

# Number of monthly hipster with budget of 40
simulation2['month'] = simulation2['time'].str[:7]

byhipster2 = simulation2.groupby(by=['month','type']).count()
byhipster2 = byhipster2.unstack()['time']
byhipster2['hipster'].fillna(0, inplace=True)

x2 = byhipster2.index[::4]
y2 = byhipster2['hipster'][::4]

plt.plot(x2, y2)

blue_patch = mpatches.Patch(color='blue', label='hipster budget is 500')
orange_patch = mpatches.Patch(color='orange', label='hipster budget is 40')
plt.legend(handles=[blue_patch, orange_patch])

plt.title("Monthly number of hipster")
plt.show()
