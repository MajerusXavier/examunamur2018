import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from Code_Part_3.PriceFunction import dprices, fprices, dprices2015, fprices2015

simulation = pd.read_csv("../Data/DataSimulation.csv", sep=";")

simulation2 = pd.read_csv("../Data/Inflation.csv", sep=";")

# Plots: Average income per day

# For the data set
    # Create a column amount
simulation['month'] = simulation['time'].str[:7]
Amount = []

for i, j in enumerate(simulation['time']):
    total = fprices(simulation['food'][i]) + dprices(simulation['drinks'][i]) + + simulation['tip'][i]
    Amount.append(total)

simulation['amount'] = Amount

    # First part of comparative plot
y = (simulation.groupby('month').sum())['amount'][::6]
x = (simulation.groupby('month', as_index=False).count())['month'][::6]

plt.plot(x, y)


# For the simulation
    # Create a column amount
simulation2['month'] = simulation2['time'].str[:7]
Amount2 = []

for k, l in enumerate(simulation2['time'][:124831]):
    total = fprices(simulation2['food'][k]) + dprices(simulation2['drinks'][k]) + simulation2['tip'][k]
    Amount2.append(total)
for m, n in enumerate(simulation2['time'][124831:]):
    total = fprices2015(simulation2['food'][m]) + dprices2015(simulation2['drinks'][m]) + simulation2['tip'][m]
    Amount2.append(total)


simulation2['amount'] = Amount2

    # Second part of the comparative plot
y2 = (simulation2.groupby('month').sum())['amount'][::6]
x2 = (simulation2.groupby('month', as_index=False).count())['month'][::6]

# legend for the graph
blue_patch = mpatches.Patch(color='blue', label='first simulation monthly income')
orange_patch = mpatches.Patch(color='orange', label='simulation inflation monthly income')
plt.legend(handles=[blue_patch, orange_patch])


plt.title('average monthly income')
plt.plot(x2, y2)
plt.show()



# Global mean of daily income


print(y.mean())
print(y2.mean())
