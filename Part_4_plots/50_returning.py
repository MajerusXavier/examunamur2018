import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

simulation = pd.read_csv("../Data/DataSimulation.csv", sep=";")
simulation2 = pd.read_csv("../Data/50_returning.csv", sep=";")

# Returning client  if 1000 in total
simulation['month'] = simulation['time'].str[:7]
simulation['returning'] = simulation['id'].str[0:1]

byreturning = simulation.groupby(['month', 'returning']).count()
byreturning = byreturning.unstack()['time']
byreturning['R'].fillna(0.0, inplace=True)

x = byreturning.index[:10]
y = byreturning['R'][:10]

plt.plot(x, y)

# Returning client if only 50 in total

simulation2['month'] = simulation2['time'].str[:7]
simulation2['returning'] = simulation2['id'].str[0:1]

byreturning2 = simulation2.groupby(['month', 'returning']).count()
byreturning2 = byreturning2.unstack()['time']
byreturning2['R'].fillna(0.0, inplace=True)

x2 = byreturning2.index[:10]
y2 = byreturning2['R'][:10]

plt.plot(x2, y2)

blue_patch = mpatches.Patch(color='blue', label='if 1000 returning')
orange_patch = mpatches.Patch(color='orange', label='if 50 returning')
plt.legend(handles=[blue_patch, orange_patch])

plt.title("Number of monthly returning client")
plt.show()
