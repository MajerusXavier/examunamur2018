import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

simulation = pd.read_csv("../Data/DataSimulation.csv", sep=";")
simulation2 = pd.read_csv("../Data/Inflation.csv", sep=";")

# Returning client of the first simulation
simulation['month'] = simulation['time'].str[:7]
simulation['returning'] = simulation['id'].str[0:1]

byreturning = simulation.groupby(['month', 'returning']).count()
byreturning = byreturning.unstack()['time']
byreturning['R'].fillna(0.0, inplace=True)

x = byreturning.index[23::3]
y = byreturning['R'][23::3]

plt.plot(x, y)

# Returning client for the simulation with inflation

simulation2['month'] = simulation2['time'].str[:7]
simulation2['returning'] = simulation2['id'].str[0:1]

byreturning2 = simulation2.groupby(['month', 'returning']).count()
byreturning2 = byreturning2.unstack()['time']
byreturning2['R'].fillna(0.0, inplace=True)

x2 = byreturning2.index[23::3]
y2 = byreturning2['R'][23::3]

plt.plot(x2, y2)

blue_patch = mpatches.Patch(color='blue', label='first simulation monthly income')
orange_patch = mpatches.Patch(color='orange', label='simulation inflation monthly income')
plt.legend(handles=[blue_patch, orange_patch])

plt.title("Number of monthly returning client")
plt.show()
