def dprices(drink):
    if drink == "frappucino":
        return 4.0
    elif drink == "milkshake":
        return 5.0
    elif drink == "water":
        return 2.0
    elif drink == "soda":
        return 3.0
    elif drink == "coffee":
        return 3.0
    elif drink == "tea":
        return 3.0


def dprices2015(drink):
    if drink == "frappucino":
        return 4.8
    elif drink == "milkshake":
        return 6.0
    elif drink == "water":
        return 2.4
    elif drink == "soda":
        return 3.6
    elif drink == "coffee":
        return 3.6
    elif drink == "tea":
        return 3.6


def fprices(food):
    if food == "sandwich":
        return 5.0
    elif food == "cookie":
        return 2.0
    elif food == "nothing":
        return 0.0
    elif food == "pie":
        return 3.0
    elif food == "muffin":
        return 3.0


def fprices2015(food):
    if food == "sandwich":
        return 6.0
    elif food == "cookie":
        return 2.4
    elif food == "nothing":
        return 0.0
    elif food == "pie":
        return 3.6
    elif food == "muffin":
        return 3.6
