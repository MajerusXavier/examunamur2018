import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from Code_Part_3.PriceFunction import dprices, fprices

DataSet = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";").\
    rename(columns={'TIME': 'time', 'CUSTOMER': 'customer', 'DRINKS': 'drinks', 'FOOD': 'food'})

DataSet['food'].fillna("nothing", inplace=True)

simulation = pd.read_csv("../Data/DataSimulation.csv", sep=";")

# Plots: Average income per day

# For the data set
    # Create a column amount
DataSet['date'] = DataSet['time'].str[0:10]
Amount = []

for i, j in enumerate(DataSet['time']):
    total = fprices(DataSet['food'][i]) + dprices(DataSet['drinks'][i])
    Amount.append(total)

DataSet['amount'] = Amount

    # First part of comparative plot
y = (DataSet.groupby('date').sum())['amount'][::365]
x = (DataSet.groupby('date', as_index=False).count())['date'][::365]

plt.plot(x, y)


# For the simulation
    # Create a column amount
simulation['date'] = simulation['time'].str[0:10]
Amount2 = []

for k, l in enumerate(simulation['time']):
    total = fprices(simulation['food'][k]) + dprices(simulation['drinks'][k]) + simulation['tip'][k]
    Amount2.append(total)

simulation['amount'] = Amount2

    # Second part of the comparative plot
y2 = (simulation.groupby('date').sum())['amount'][::365]
x2 = (simulation.groupby('date', as_index=False).count())['date'][::365]

# legend for the graph
blue_patch = mpatches.Patch(color='blue', label='dataset daily income')
orange_patch = mpatches.Patch(color='orange', label='simulation daily income')
plt.legend(handles=[blue_patch, orange_patch])


plt.title('average daily income on the first january (€)')
plt.plot(x2, y2)
plt.show()


# Global mean of daily income
print("The average daily income of the dataset is: %s €" % (y.mean()))
print("The average daily income of the simulation is: %s €" % (y2.mean()))




