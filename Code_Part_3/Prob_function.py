import pandas as pd

path = "../Data/"
DataSet = pd.read_csv(path+"Coffeebar_2013-2017.csv", sep=";").\
    rename(columns={'TIME': 'time', 'CUSTOMER': 'customer', 'DRINKS': 'drinks', 'FOOD': 'food'})

DataSet['hours'] = DataSet['time'].str[11:16]

# To get all probabilities, including when they buy no food
DataSet['food'].fillna("nothing", inplace=True)

# Probabilities in series
prob_drink = (DataSet.groupby(['hours', 'drinks']).count() / DataSet.groupby('hours').count())['time']
prob_food = (DataSet.groupby(['hours', 'food']).count() / DataSet.groupby('hours').count())['time']

# To get every probabilities, including when value is 0
prob_food = prob_food.unstack().fillna(0)
prob_drink = prob_drink.unstack().fillna(0)


# Functions of probabilities as serie and has "get me the right prob"


def probdrink():
    return prob_drink


def probfood():
    return prob_food


def pdrink(hour, drink):
    return prob_drink[drink].loc[hour]


def pfood(hour, food):
    return prob_food[food].loc[hour]

