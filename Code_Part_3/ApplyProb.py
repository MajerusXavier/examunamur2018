import random as rd
from Code_Part_3.Prob_function import pdrink, probdrink, pfood, probfood


def applydprobdrink(hour):
    number = rd.randint(1, 99)
    prob = 0

    for s in probdrink():
        if number <= (prob + pdrink(hour, s)) * 100:
            return s

        else:
            prob += pdrink(hour, s)


def applyprobfood(hour):
    number = rd.randint(1, 99)
    prob = 0

    for s in probfood():
        if number <= (prob + pfood(hour, s)) * 100:
            return s

        else:
            prob += pfood(hour, s)
